﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer aTimer;
        public double Interval
        {
            get
            {
                return aTimer.Interval;
            }
            set
            {
                aTimer.Interval = value*1000;
            }
        }

        public event ElapsedEventHandler Elapsed
        {
            add
            {
                aTimer.Elapsed += value;
            }

            remove
            {
                aTimer.Elapsed -= value;
            }
        }

        public void Dispose()
        {
            aTimer.Dispose();
        }

        public void Start()
        {

            aTimer.Start();
        }

        public void Stop()
        {
            aTimer.Stop();
        }
        public TimerService()
        {
            aTimer = new Timer();
        }
    }
}