﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using ParkingLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        readonly Parking _parking;
        public List<TransactionInfo> Tracsactions = new();
        private static ParkingService instance = new ParkingService(new TimerService(), new TimerService(), new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"));


        public ParkingService(ITimerService WithdrawTimer,ITimerService LogTimer,ILogService LogService)
        {
            _withdrawTimer = WithdrawTimer;
            _withdrawTimer.Interval = Settings.PaymentTimer;
            _withdrawTimer.Elapsed += Timer;
            _withdrawTimer.Start();
            _logTimer = LogTimer;
            _logTimer.Interval = Settings.RetentionTimer;
            _logTimer.Elapsed += SaveTransactionsToLog;
            _logTimer.Start();
            _logService = LogService;
            _parking = Parking.GetInstance();
        }
        
        public void SaveTransactionsToLog(Object source, System.Timers.ElapsedEventArgs e)
        {
            var transac = GetLastParkingTransactions();
            foreach (TransactionInfo tranc in transac)
            {
                var log = $"{tranc.Date.ToUniversalTime()} - transfer {tranc.Sum} from Vehicle {tranc.VehicleId}";
                _logService.Write(log);
                Tracsactions.Remove(tranc);
            }
            
        }
        public string ReadTransactionsFromLog()
        {
            return _logService.Read();
        }
        public void Timer(Object source, System.Timers.ElapsedEventArgs e)
        {
            _parking.GetVehicles.ForEach(x => DebitMoney(x));
        }
        public void DebitMoney(Vehicle v)
        {
            var payment = GetPayment(v);
            TransactionInfo t = new(v.Id, payment);
            _parking.Balance += t.Sum;
            v.Balance -= payment;

            Tracsactions.Add(t);

         }
        private decimal GetPayment(Vehicle v)
        {
            var found = Settings.Prices.TryGetValue(v.Type, out decimal price);
            if (!found)
            {
                throw new ArgumentException($"Type {(int)v.Type} not found");
            }
            if (v.Balance >= price)
            {
                return price;
            }
            else if (v.Balance <= 0)
            {
                return Settings.Penalty * price;
            }
            else
            {
                decimal amount1, amount2;
                amount1 = v.Balance;
                amount2 = (price - v.Balance)*Settings.Penalty;
                return amount1 + amount2;

            }
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() > 0)
            {
                if (!_parking.GetVehicles.Exists(x => x.Id == vehicle.Id))
                {
                    _parking.AddVehicle(vehicle);
                }
                else
                {
                    throw new ArgumentException ("Vehicle already on parking.");
                }
                
            }
            else
            {
                throw new InvalidOperationException("There is no free space on the parking.");
            }
        }

        public decimal GetBalance()
        {
            decimal sum = 0;
            var transac = GetLastParkingTransactions();
            foreach (TransactionInfo tranc in transac)
                sum += tranc.Sum;
            return sum;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.FreePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Tracsactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.GetVehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return ReadTransactionsFromLog();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (_parking.GetVehicles.Exists(x => x.Id == vehicleId))
            {
                Vehicle v = _parking.GetVehicles.Find(x => x.Id == vehicleId);
                if(v.Balance>=0)
                    _parking.RemoveVehicle(v);
                else
                    throw new InvalidOperationException("Vehicle with the negative balance cannot be removed.");
            }
            else
            {
                throw new ArgumentException("Vehicle not on parking.");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum > 0)
            { 
                if (_parking.GetVehicles.Exists(x => x.Id == vehicleId))
                {
                    Vehicle v = _parking.GetVehicles.Find(x => x.Id == vehicleId);
                    v.AddBalance(sum);
                }
                else
                {
                    throw new ArgumentException("Vehicle not on parking.");
                }
            }
            else
            {
                throw new ArgumentException("Sum must be positive.");
            }
        }

    
        public void Dispose()
        {
            _parking.Dispose();
        }
        public static ParkingService Instance { get { return instance; } }
    }
}