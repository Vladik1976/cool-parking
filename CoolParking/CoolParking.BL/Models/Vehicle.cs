﻿                                                                                                                                                                                       // TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string _VehicleId;
        private decimal _Balance;
        private VehicleType _VehicleType;
        private static string _ParkingLotId = GenerateRandomRegistrationPlateNumber();
        public string Id
        { 
            get
            {
                return _VehicleId;
            }
         }
        public decimal Balance {
            get
            {
                return _Balance;
            } 
            set
            {
                _Balance = value;
            }
        }
        public VehicleType Type {
            get
            {
                return _VehicleType;
            }
         }
        private string ParkingLotId {
            get
            {
                return _ParkingLotId;
            } 
        }
        private bool CheckVehicleID(string VehicleID)
        {
            Regex regex = new Regex(@"^[A-Z]{2}\-?\d{4}\-[A-Z]{2}");
            return regex.IsMatch(VehicleID);
        }
        public Vehicle(string VehicleId, VehicleType VehicleType, decimal Balance)
        {
            if (CheckVehicleID(VehicleId))
            {
                if (Balance > 0)
                {
                    _VehicleType = VehicleType;
                    _VehicleId = VehicleId;
                    _Balance = Balance;
                }
                else
                {
                    throw new ArgumentException("Wrong Balance. Should be positive");
                }
            }
            else
            {
                throw new ArgumentException("Wrong Vehicle ID. Should be in format AA-XXXX-AA");
            }
        }

        public void AddBalance(decimal Balance)
        {
            _Balance += Balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}