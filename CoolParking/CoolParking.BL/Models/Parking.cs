﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using ParkingLibrary;
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        readonly List<Vehicle> Vehicles=new();
        private static Parking instance;


        public Parking()
        {
            
        }
        public static Parking GetInstance()
        {
            if (instance == null)
            {
                instance = new Parking();
                instance.Capacity= Settings.ParkingSpace;
                instance.FreePlaces= Settings.ParkingSpace;
            }
            return instance;
        }
        public int Capacity{get;set;}
        public int FreePlaces { get; set; }
        public decimal Balance { get; set; }
        public List<Vehicle> GetVehicles
        {
            get
            {
                return Vehicles;
            }
        }
        public void AddVehicle(Vehicle Vehicle)
        {
            Vehicles.Add(Vehicle);
            FreePlaces--;
        }
        public void RemoveVehicle(Vehicle Vehicle)
        {
            Vehicles.Remove(Vehicle);
            FreePlaces++;
        }

        public void Dispose()
        {
            instance = null;
        }
    }
}