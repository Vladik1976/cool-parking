﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;

namespace ParkingLibrary
{
    public static class Settings
    {
        public readonly static string File = "Transactions.log";
        public readonly static int PaymentTimer =5;
        public readonly static int RetentionTimer = 60;
        public readonly static Dictionary<VehicleType, decimal> Prices = new ()
        {
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Truck] = 5,
            [VehicleType.PassengerCar] = 2,
            [VehicleType.Motorcycle] = 1,

        };
        public readonly static int ParkingSpace = 10;
        public readonly static decimal Penalty = 2.5M;


    }
}