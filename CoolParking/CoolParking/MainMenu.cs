﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking
{
    class MainMenu
    {
        private static MainMenu instance = new();

        public void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("0 - Вивести на екран поточний баланс Паркінгу.");
            Console.WriteLine("1 - Вивести на екран суму зароблених коштів за поточний період (до запису у лог).");
            Console.WriteLine("2 - Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).");
            Console.WriteLine("3 - Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог)");
            Console.WriteLine("4 - Вивести на екран історію Транзакцій (зчитавши дані з файлу Transactions.log)");
            Console.WriteLine("5 - Вивести на екран список Тр. засобів , що знаходяться на Паркінгу");
            Console.WriteLine("6 - Поставити Транспортний засіб на Паркінг");
            Console.WriteLine("7 - Забрати Транспортний засіб з Паркінгу");
            Console.WriteLine("8 - Поповнити баланс конкретного Тр. засобу");
        }
        public void AddBalance()
        {
            Console.WriteLine("Введіть режстраційний номер тр. засобу");
            string idcar = Console.ReadLine();
            Console.WriteLine("Введіть суму поповнення");
            decimal balance = Convert.ToInt32(Console.ReadLine());
            try
            {
                ParkingService.Instance.TopUpVehicle(idcar, balance);
            }
            catch ( InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }

        }
        public void ShowListOfVehicles()
        {
            foreach (Vehicle v in ParkingService.Instance.GetVehicles())
                Console.WriteLine($"Id {v.Id}, type {v.Type}, balance{v.Balance}");
            
        }
        public void ShowTransactionsLog()
        {
            string Log = ParkingService.Instance.ReadTransactionsFromLog();
            Console.Write(Log);
        }

        public void ShowTransactions()
        {
            var transac =  ParkingService.Instance.GetLastParkingTransactions();
            foreach (TransactionInfo tranc in transac)
                Console.WriteLine($"Id {tranc.VehicleId}, date {tranc.Date}, paid: ${tranc.Sum} ");
        }

        public void CountFreeSpace()
        {
            Console.WriteLine("На парковці вільно " + ParkingService.Instance.GetFreePlaces().ToString() + " з " + ParkingService.Instance.GetCapacity().ToString());
        }

        public void IncorectInput()
        {
            Console.WriteLine("Incorect input");
        }
        public void AddNewcar()
        {
            SelectTypeCar();
            var input = Console.ReadLine();
            if (int.TryParse(input, out int a))
            {
                Console.WriteLine("Реєстраційний номер в форматі АА-0000-АА");
                var Reg = Console.ReadLine();
                Console.WriteLine("Початковий баланс");
                var Bal = Console.ReadLine();
                if (decimal.TryParse(Bal, out decimal b))
                {
                    int type = Convert.ToInt32(input);
                    decimal Balance = Convert.ToDecimal(Bal);
                    Vehicle v = new Vehicle(Reg, (VehicleType)type, Balance);
                    try
                    {
                        ParkingService.Instance.AddVehicle(v);
                        Console.WriteLine("Автомобіль додано ");
                    }
                    catch(InvalidOperationException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    
                }
            }
        }

        public void SelectTypeCar()
        {
            Console.WriteLine("Тип транспортного засобу");
            Console.WriteLine("1 - Автомобіль");
            Console.WriteLine("2 - Вантажівка");
            Console.WriteLine("3 - Автобус");
            Console.WriteLine("4 - Мотоцикл\n");
            Console.WriteLine("й - Меню");
        }

        public void ShowParkingBalance()
        {
            Console.WriteLine("Поточний баланс паркінгу " + ParkingService.Instance.GetBalance());
        }
        public void RemoveCar()
        {
            Console.WriteLine("Введіть реєстраційний номер");
            string id = Console.ReadLine();
            try
            {
                ParkingService.Instance.RemoveVehicle(id);
                Console.WriteLine("Автомобіль іидалено з паркінгу");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static MainMenu Instance { get { return instance; } }
    }
}
