﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkingService p = new ParkingService(new TimerService(), new TimerService(), new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"));
            while (true)
            {
                MainMenu.Instance.BasicMenu();

                var input = Console.ReadKey();
                Console.WriteLine();
                switch (input.KeyChar)
                {
                    case '0':
                        MainMenu.Instance.ShowParkingBalance();
                        break;
                    case '1':
                        MainMenu.Instance.ShowParkingBalance();
                        break;
                    case '2':
                        MainMenu.Instance.CountFreeSpace();
                        break;
                    case '3':
                        MainMenu.Instance.ShowTransactions();
                        break;
                    case '4':
                        MainMenu.Instance.ShowTransactionsLog();
                        break;
                    case '5':
                        MainMenu.Instance.ShowListOfVehicles();
                        break;
                    case '6':
                        MainMenu.Instance.AddNewcar();
                        break;
                    case '7':
                        MainMenu.Instance.RemoveCar();
                        break;
                    case '8':
                        MainMenu.Instance.AddBalance();
                        break;
                    case 'q':
                        Console.Clear();
                        break;
                    default:
                        MainMenu.Instance.IncorectInput();
                        break;
                }

            }
        }

        
    }
}
